//
//  WaveView.swift
//  Pods-XFYAnimation_Example
//
//  Created by 🐑 on 2019/5/25.
//

import UIKit

open class WaveViewConfigure: NSObject {
    ///波浪颜色
    open var color: UIColor = UIColor(white: 1, alpha: 0.6)
    ///水纹振幅
    open var amplitude: CGFloat = 12
    ///水纹周期
    open var cycle: CGFloat = 0.5 / 30.0
    ///波浪Y
    open var y: CGFloat = 0
    ///水纹速度
    open var speed: CGFloat = 0.05
    ///水纹宽度
    open var width: CGFloat = 0
    ///上升速度
    open var upSpeed: CGFloat = 0
}

open class WaveView: UIView {
    
    private var configure = WaveViewConfigure()
    private let shapeLayer: CAShapeLayer = CAShapeLayer()
    private var offsetX: CGFloat = 0
    private var displayLink: CADisplayLink?
    
    @IBInspectable var amplitude: CGFloat {
        get {
            return configure.amplitude
        }
        set {
            configure.amplitude = newValue
        }
    }
    
    @IBInspectable var cycle: CGFloat {
        get {
            return configure.cycle
        }
        set {
            configure.cycle = newValue
        }
    }
    
    @IBInspectable var starY: CGFloat {
        get {
            return configure.y
        }
        set {
            configure.y = newValue
        }
    }
    
    @IBInspectable var speed: CGFloat {
        get {
            return configure.speed
        }
        set {
            configure.speed = newValue
        }
    }
    
    @IBInspectable var upSpeed: CGFloat {
        get {
            return configure.upSpeed
        }
        set {
            configure.upSpeed = newValue
        }
    }
    
    @IBInspectable var color: UIColor {
        get {
            return configure.color
        }
        set {
            configure.color = newValue
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    func initView() {
        configure.width = bounds.width
        if configure.y == 0 {
            configure.y = configure.amplitude
        }
        backgroundColor = .clear
        shapeLayer.fillColor = configure.color.cgColor
        layer.addSublayer(shapeLayer)
    }
    
    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow != nil {
            startAnimation()
        } else {
            invalidate()
        }
    }
    
    @objc private func currentWave() {
        if configure.amplitude == 0 && configure.y == 0 && configure.upSpeed != 0 {
            invalidate()
        } else {
            offsetX = offsetX + configure.speed
            configure.y = max(configure.y - configure.upSpeed, 0)
            if (configure.y < configure.amplitude) {
                configure.amplitude = configure.y;
            }
            currentFirstWaveLayerPath()
        }
    }
    
    private func currentFirstWaveLayerPath() {
        //创建一个路径
        let path: CGMutablePath = CGMutablePath()
        var y: CGFloat = configure.y
        path.move(to: CGPoint.init(x: 0, y: y))
        for i in 0...Int(configure.width) {
            y = configure.amplitude * sin(configure.cycle * CGFloat(i) + CGFloat(offsetX)) + configure.y
            //将点连成线
            path.addLine(to: CGPoint(x: CGFloat(i), y: y), transform: .identity)
        }
        path.addLine(to: CGPoint.init(x: configure.width, y: frame.size.height))
        path.addLine(to: CGPoint.init(x: 0, y: frame.size.height))
        path.closeSubpath()
        shapeLayer.path = path
    }
}

public extension WaveView {
    
    ///更新基本配置，block不会造成循环引用
    func updateWithConfigure(_ configureBlock: ((WaveViewConfigure) -> Void)?) {
        configure.width = frame.size.width
        configureBlock?(configure)
        shapeLayer.fillColor = configure.color.cgColor
        startAnimation()
    }
    
    ///开始动画
    func startAnimation() {
        invalidate()
        displayLink = CADisplayLink(target: self, selector: #selector(currentWave))
        //启动定时器
        displayLink?.add(to: .current, forMode: RunLoop.Mode.common)
    }
    
    ///销毁
    func invalidate() {
        displayLink?.invalidate()
        displayLink = nil
    }
}
