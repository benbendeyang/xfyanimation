//
//  RotateAnimationView.swift
//  Pods-XFYAnimation_Example
//
//  Created by 🐑 on 2019/5/24.
//

import UIKit

open class RotateAnimationViewConfigure: NSObject {
    
    ///开始起点
    open var startAngle: CGFloat = -(.pi * 0.5)
    ///开始结束点
    open var endAngle: CGFloat = .pi * 1.5
    ///动画总时间
    open var duration: TimeInterval = 2
    ///动画间隔时间
    open var intervalDuration: TimeInterval = 0.12
    ///小球个数
    open var number: NSInteger = 5
    ///小球直径
    open var diameter: CGFloat = 8
    ///小球背景颜色
    open var backgroundColor: UIColor = UIColor.red
}


open class RotateAnimationView: UIView {
    
    private var layerArray: Array<CALayer> = Array()
    private var isPause: Bool = false
    private var configure: RotateAnimationViewConfigure = RotateAnimationViewConfigure()
    
    @IBInspectable var duration: CGFloat {
        get {
            return CGFloat(configure.duration)
        }
        set {
            configure.duration = TimeInterval(newValue)
        }
    }
    
    @IBInspectable var intervalDuration: CGFloat {
        get {
            return CGFloat(configure.intervalDuration)
        }
        set {
            configure.intervalDuration = TimeInterval(newValue)
        }
    }
    
    @IBInspectable var number: NSInteger {
        get {
            return configure.number
        }
        set {
            configure.number = newValue
        }
    }
    
    @IBInspectable var diameter: CGFloat {
        get {
            return configure.diameter
        }
        set {
            configure.diameter = newValue
        }
    }
    
    @IBInspectable var color: UIColor {
        get {
            return configure.backgroundColor
        }
        set {
            configure.backgroundColor = newValue
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    func initView() {
        startAnimation()
    }
}

public extension RotateAnimationView {
    
    func updateWithConfigure(_ configureBlock: ((RotateAnimationViewConfigure) -> Void)?) {
        configureBlock?(configure)
        startAnimation()
    }
    
    ///开始动画
    func startAnimation() {
        let intervalDuration: CGFloat = CGFloat(CGFloat(configure.duration) / 2.0 / CGFloat(configure.number))
        configure.intervalDuration = min(configure.intervalDuration, TimeInterval(intervalDuration))
        
        for item in layerArray {
            item.removeFromSuperlayer()
        }
        layerArray.removeAll()
        
        for item in 0 ..< configure.number {
            //创建layer
            let scale: CGFloat = CGFloat(configure.number + 1 - item) / CGFloat(configure.number + 1)
            let layer: CALayer = CALayer()
            layer.backgroundColor = configure.backgroundColor.cgColor
            let layerLength = scale * configure.diameter
            layer.frame = CGRect(x: bounds.midX - layerLength/2, y: (configure.diameter - layerLength)/2, width: layerLength, height: layerLength)
            layer.cornerRadius = layerLength/2
            //运动路径
            let pathAnimation: CAKeyframeAnimation = CAKeyframeAnimation.init(keyPath: "position")
            pathAnimation.calculationMode = .paced
            pathAnimation.fillMode = .forwards
            pathAnimation.isRemovedOnCompletion = false
            pathAnimation.duration = (configure.duration) - Double((configure.intervalDuration) * Double(configure.number))
            pathAnimation.beginTime = Double(item) * configure.intervalDuration
            pathAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            pathAnimation.path = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: (frame.size.width - configure.diameter)/2, startAngle: configure.startAngle, endAngle: configure.endAngle, clockwise: true).cgPath
            //动画组，动画组时间长于单个动画时间，会有停留效果
            let group: CAAnimationGroup = CAAnimationGroup()
            group.animations = [pathAnimation]
            group.duration = configure.duration
            group.isRemovedOnCompletion = false
            group.fillMode = .forwards
            group.repeatCount = MAXFLOAT
            
            layer.add(group, forKey: "RotateAnimation")
            layerArray.append(layer)
            self.layer.addSublayer(layer)
        }
    }
    ///停止动画
    func stopAnimation() {
        for item in layerArray {
            item.removeFromSuperlayer()
        }
        layerArray.removeAll()
    }
    ///暂停动画
    func pauseAnimation() {
        if isPause { return }
        isPause = true
        //取出当前时间,转成动画暂停的时间
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        //设置动画运行速度为0
        layer.speed = 0.0
        //设置动画的时间偏移量，指定时间偏移量的目的是让动画定格在该时间点的位置
        layer.timeOffset = pausedTime
    }
    ///恢复动画
    func resumeAnimation() {
        if !isPause { return }
        isPause = false
        //获取暂停的时间差
        let pausedTime = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        //用现在的时间减去时间差,就是之前暂停的时间,从之前暂停的时间开始动画
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
    }
}
