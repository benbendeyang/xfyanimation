//
//  SubViewController.swift
//  XFYAnimation_Example
//
//  Created by 🐑 on 2019/5/24.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import XFYAnimation

class SubViewController: UIViewController {

    let animationView = RotateAnimationView(frame: CGRect(x: 100, y: 150, width: 50, height: 50))
    var waveView: WaveView = WaveView(frame: CGRect(x: 0, y: 150, width: UIScreen.main.bounds.width, height: 150))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(animationView)

        view.addSubview(waveView)
    }
    
    @IBAction func play(_ sender: Any) {
        animationView.updateWithConfigure { cofigure in
            cofigure.number = 8
            cofigure.backgroundColor = .orange
        }
        
        waveView.updateWithConfigure { cofigure in
            cofigure.y = 120
            cofigure.upSpeed = 0.1
            cofigure.amplitude = 12
        }
    }
    @IBAction func stop(_ sender: Any) {
        animationView.stopAnimation()
        
        waveView.invalidate()
    }
    @IBAction func pause(_ sender: Any) {
        animationView.pauseAnimation()
    }
    @IBAction func resume(_ sender: Any) {
        animationView.resumeAnimation()
    }
    deinit {
        print("销毁")
    }
}
