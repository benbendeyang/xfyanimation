# XFYAnimation

[![CI Status](https://img.shields.io/travis/leonazhu/XFYAnimation.svg?style=flat)](https://travis-ci.org/leonazhu/XFYAnimation)
[![Version](https://img.shields.io/cocoapods/v/XFYAnimation.svg?style=flat)](https://cocoapods.org/pods/XFYAnimation)
[![License](https://img.shields.io/cocoapods/l/XFYAnimation.svg?style=flat)](https://cocoapods.org/pods/XFYAnimation)
[![Platform](https://img.shields.io/cocoapods/p/XFYAnimation.svg?style=flat)](https://cocoapods.org/pods/XFYAnimation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYAnimation is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYAnimation'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYAnimation is available under the MIT license. See the LICENSE file for more info.
